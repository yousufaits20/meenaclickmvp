package com.example.meenaclickmvp.ui.cart

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.apiretrofitdemo.Utils.Constant
import com.example.meenaclickmvp.R
import com.example.meenaclickmvp.model.CatalogProductsItem
import com.example.meenaclickmvp.session.SharedPreferencesImpl
import kotlinx.android.synthetic.main.cart_item_row.view.*

class CartAdapter(
    val cartItem: ArrayList<CatalogProductsItem?>, val context: CartActivity,val
    context1: SharedPreferencesImpl
) :
    RecyclerView.Adapter<CartAdapter.ViewHolder>(){
    lateinit var presenter: CartPresenter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        presenter = CartPresenter(context, context1)
        val v = LayoutInflater.from(context).inflate(R.layout.cart_item_row, parent, false)
        return  ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return cartItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        Glide.with(context).load(Constant.IMAGE_BASE_URL + cartItem.get(position)!!.productImage)
            .into(holder.cart_item_image)
        holder.cart_item_name.text = cartItem.get(position)!!.productName
        holder.cart_item_weight.text =
            cartItem.get(position)!!.weight + " " + cartItem.get(position)!!.weightClass
        holder.cart_item_price.text = Constant.tk + cartItem.get(position)!!.price.toString()
        holder.cart_item_qty.text = cartItem.get(position)!!.cartQty.toString()

        /*//for initial product quantity
        var qtn = holder.cart_item_qty.text.toString().toInt()
        if (qtn < 1){
            holder.minus_btn.setImageResource(R.drawable.ic_delete)
        }*/


        //when click delete
        holder.cart_item_delete.setOnClickListener {
            removeItemFromCart(position)
        }

        //when click on plus btn
        holder.plus_btn.setOnClickListener {
            var updateCartValue = holder.cart_item_qty.text.toString().toInt()
            updateCartValue++

            //update in session & set on display
            presenter.updateCartQty(updateCartValue!!, cartItem[position]!!)
            holder.cart_item_qty.text = "$updateCartValue"

            //check qty > 1 & set minus btn
            if(updateCartValue > 1){
                holder.minus_btn.setImageResource(R.drawable.minus_21)
            }

            //call price calculator
            presenter.calculateCartPrice()
        }

        //click on minus btn
        holder.minus_btn.setOnClickListener {

            var cartPresentValue = holder.cart_item_qty.text.toString().toInt()

            if (cartPresentValue > 1){
                cartPresentValue --
                presenter.updateCartQty(cartPresentValue, cartItem[position]!!)
                holder.cart_item_qty.text = "$cartPresentValue"
            }

            //check qty == 1
            if (cartPresentValue == 1){
                holder.minus_btn.setImageResource(R.drawable.ic_delete)
                holder.minus_btn.setOnClickListener {
                    removeItemFromCart(position)
                }
            }

            //check qty == 0 &
               //then remove item from cart
            /*if (cartPresentValue == 0){

            }*/

            //update cart price
            presenter.calculateCartPrice()
        }

    }

    private fun removeItemFromCart(position: Int) {
        cartItem[position]?.let { it1 -> context1.removeProduct(it1) }
        //decrease cart item size before remove from list
        val cartQty = cartItem.size - 1
        cartItem.remove(cartItem[position])
        notifyDataSetChanged()

        //update toolbar value
        context.getCartValue(cartQty)
    }


        class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val cart_item_image = itemView.iv_image_cart
            val cart_item_name = itemView.tv_name
            val cart_item_weight = itemView.tv_weight
            val cart_item_price = itemView.tv_price
            val cart_item_qty = itemView.tv_cart_count!!
            val cart_item_delete = itemView.iv_delete
            val plus_btn = itemView.iv_plus
            val minus_btn = itemView.iv_minus
        }
}


