package com.example.meenaclickmvp.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.ImageButton
import androidx.appcompat.widget.AppCompatTextView
import com.example.meenaclickmvp.R
import com.example.meenaclickmvp.session.Session
import com.example.meenaclickmvp.session.SharedPreferencesImpl
import com.example.meenaclickmvp.ui.cart.CartActivity
import com.example.meenaclickmvp.ui.home.FragmentToActivityCom
import com.example.meenaclickmvp.ui.home.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.View, FragmentToActivityCom{
    lateinit var cartValue: AppCompatTextView
    lateinit var session: Session
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter = MainPresenter(this, this)
        session = SharedPreferencesImpl(this)
        //load initial fragment
        loadFragment(HomeFragment())

        //set toolbar
        setToolbar()
    }

    private fun setToolbar() {
        toolbar.setTitle("Cereals")
        toolbar.setNavigationIcon(R.drawable.back)
        setSupportActionBar(toolbar)
    }

    private fun loadFragment(homeFragment: HomeFragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragmentContainer, homeFragment)
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_search_cart, menu)
        val menuItem = menu?.findItem(R.id.menu_item_search)
        search_view.setMenuItem(menuItem)
        getCartValue(menu)

        return  true
    }

    private fun getCartValue(menu: Menu?) {
        val cartMain = menu!!.findItem(R.id.menu_item_cart).actionView
        val cartBtn = cartMain.findViewById<ImageButton>(R.id.button_cart)
        cartValue = cartMain!!.findViewById(R.id.tv_notification_count)
        /*cartValue.text = "${session.getCartAllProduct().size}"*/

        //call set toolbar
        presenter.getToolbarValue()

        navigateToCartActivity(cartBtn)
    }

    private fun navigateToCartActivity(cartBtn: ImageButton) {
        cartBtn.setOnClickListener {
            val intent = Intent(this, CartActivity::class.java)
            startActivity(intent)
        }

        /*cartMain.setOnClickListener {
        val intent = Intent(this, CartActivity::class.java)
        startActivity(intent)
    }*/
    }

    override fun setToolbarValue(value: Int) {
        cartValue.text = "$value"
    }

    override fun updatetoolbar() {


    }

}
