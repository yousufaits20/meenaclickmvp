package com.example.meenaclickmvp.ui

class MainContract {
    interface View{
        fun setToolbarValue(value : Int)
    }
    interface Presenter{
        fun getToolbarValue()
    }
}