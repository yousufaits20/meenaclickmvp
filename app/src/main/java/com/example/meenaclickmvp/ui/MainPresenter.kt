package com.example.meenaclickmvp.ui

import android.content.Context
import com.example.meenaclickmvp.session.Session
import com.example.meenaclickmvp.session.SharedPreferencesImpl

class MainPresenter (val view: MainContract.View, context: MainActivity): MainContract.Presenter {
    var session = SharedPreferencesImpl(context)

    override fun getToolbarValue() {
        val value = session.getCartAllProduct().size
        view.setToolbarValue(value)
    }
}