package com.example.meenaclickmvp.ui.cart

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apiretrofitdemo.Utils.Constant
import com.example.meenaclickmvp.R
import com.example.meenaclickmvp.model.CatalogProductsItem
import com.example.meenaclickmvp.session.SharedPreferencesImpl
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_main.*

class CartActivity : AppCompatActivity(),CartContract.View {
    lateinit var presenter: CartPresenter
    lateinit var session : SharedPreferencesImpl
    lateinit var recyclerView: RecyclerView
    val cartValue = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        recyclerView = cartRecycler
        session = SharedPreferencesImpl(this)
        presenter = CartPresenter(this, session)
        presenter.getCartItem()

        //get cart total price
        presenter.calculateCartPrice()

        //set toolbar method
        setToolbar()
    }

    override fun setDataIntoAdapter(list: ArrayList<CatalogProductsItem?>) {
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val adapter = CartAdapter(list, this, session)
        recyclerView.adapter = adapter
    }

    //back btn
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
        return true
    }

    override fun getCartValue(value: Int) {

        //set toolbar cart value
        toolbar.setTitle("Shopping Cart ($value)")

        setEmptyView(value)
    }

    override fun setCartPrice(price: Int) {

        //set subtotal
        cart_subtotal_value_tv.text = Constant.tk + "$price"

        //set total price without apply coupon code
        cart_total_price_tv.text = Constant.tk + "$price"
        Log.e("ok", "$price")
    }

    private fun setEmptyView(value: Int) {
        if (value == 0){
            empty_cart.visibility = View.VISIBLE
        }
    }

    private fun setToolbar() {
        toolbar.setNavigationIcon(R.drawable.back)
        setSupportActionBar(toolbar)
    }
}
