package com.example.meenaclickmvp.session

import com.example.meenaclickmvp.model.CatalogProductsItem

interface Session {
    fun addToCart(productsItem: CatalogProductsItem)
    fun isProductExist(productsItem: CatalogProductsItem): Boolean
    fun getCartAllProduct(): ArrayList<CatalogProductsItem?>
    fun removeProduct(productsItem: CatalogProductsItem)
    fun updateCartQtyWithProduct(qty: Int, item: CatalogProductsItem)
}